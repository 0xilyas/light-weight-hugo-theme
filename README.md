# light weight blog theme for hugo
- no JS
- simple

## installation
```
git clone https://gitlab.com/0xilyas/light-weight-hugo-theme 
cd light-weight-hugo-theme
hugo server
```

# ScreenShot's

![screenshot 0](static/images/screenshot.png)
![screenshot 1](static/images/screenshot-1.png)
![screenshot 2](static/images/screenshot-2.png)
